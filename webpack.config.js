const { resolve } = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
  context: resolve('src'),
  entry: {
    'app'              : './app.ts',
    'pages/index/index': './pages/index/index.ts',
    'pages/logs/logs'  : './pages/logs/logs.ts'
  },
  output: {
    path: resolve('dist'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
         test: /\.js$/,
         use: 'babel-loader'
       },
      {
        test: /\.ts?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [ '.ts', '.js' ],
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
    }),
    new CopyWebpackPlugin({
    patterns: [
      {
        from: '**/*',
        to: './',
        globOptions: {
          ignore: ['**/*.ts$*' ],
        },
      },
    ]}),
  ],
  mode: 'none',
}
